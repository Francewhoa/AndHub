/*
    This file is part of the Nomad*.

    Nomad* is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Nomad* is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the Nomad*.

    If not, see <http://www.gnu.org/licenses/>.
 */
package com.dfa.hubzilla_android.util;

import com.dfa.hubzilla_android.App;
import com.dfa.hubzilla_android.R;
import com.dfa.hubzilla_android.data.DiasporaAspect;
import com.dfa.hubzilla_android.data.DiasporaPodList.DiasporaPod;

/**
 * Helper class that provides easy access to specific urls related to diaspora
 * Created by vanitasvitae on 10.08.16.
 */
@SuppressWarnings({"unused", "SpellCheckingInspection", "SameParameterValue", "WeakerAccess"})
public class DiasporaUrlHelper {
    private static final String SUBURL_INVITE = "/invite";
    private final AppSettings settings;

    public static final String URL_BLANK = "about:blank";
    public static final String SUBURL_POSTS = "/channel"; //edited for hub
    public static final String SUBURL_STREAM = "/network"; //edited for hub
    public static final String SUBURL_DIRECTORY = "/directory"; //edited for hub
  public static final String SUBURL_STREAM_WITH_TIMESTAMP = SUBURL_STREAM + "?max_time=";
    public static final String SUBURL_CONVERSATIONS = "/mail/"; //edited for hub
    public static final String SUBURL_NEW_POST = "/rpost?f=&body=";  //edited for hub
    public static final String SUBURL_PEOPLE = "/connections"; //edited for hub
    public static final String SUBURL_ACTIVITY = "/network"; //edited for hub
    public static final String SUBURL_PUBSTREAM = "/pubstream"; //edited for hub
  public static final String SUBURL_TOGGLE_MOBILE = "/mobile/toggle";
    public static final String SUBURL_SEARCH_TAGS = "/search?search=%23"; //edited for hub
    public static final String SUBURL_SEARCH_PEOPLE = "/directory?f=1&navsearch=1&search="; //edited for hub must be better
    public static final String SUBURL_SEARCH_CONTENT = "/search?search="; //added for Nomad
    public static final String SUBURL_PERSONAL_SETTINGS = "/settings"; //edited for hub
    public static final String SUBURL_SIGN_IN = "#"; //edited for hub
    public static final String SUBURL_CONTACTS = "/connections"; //edited for hub
    public static final String SUBURL_THEME = "/settings/display"; //edited for hub
    public static final String SUBURL_HOME = "/channel"; //added for hub
    public static final String SUBURL_PROFILE = "/profiles"; //added for hub
    public static final String SUBURL_EVENTS = "/events"; //added for hub
    public static final String SUBURL_MANAGE_ADDON = "/settings/featured"; //added for hub
    public static final String SUBURL_SUGGESTIONS = "/suggest"; //added for hub
    public static final String SUBURL_API = "/api/z/1.0"; //added to get api to work
    public static final String SUBURL_API_PROFILE = "/channel/export/basic"; //added to get api to work
    public DiasporaUrlHelper(AppSettings settings) {
        this.settings = settings;
    }

    /**
     * Return a url of the pod set in AppSettingsBase.
     * Eg. https://pod.geraspora.de
     *
     * @return https://(pod-domain.tld)
     */
    public String getPodUrl() {
        DiasporaPod pod = settings.getPod();
        if (pod != null) {
            return pod.getPodUrl().getBaseUrl();
        }
        return "http://127.0.0.1";

    }

    /**
     * Return a url that points to the stream of the configured diaspora account
     *
     * @return https://(pod-domain.tld)/stream
     */
    public String getStreamUrl() {
        return getPodUrl() + SUBURL_STREAM;
    }

    /**
     * Return a url that points to the stream of the configured diaspora account on a timestamp
     *
     * @return https://(pod-domain.tld)/stream?max_time=1482057867
     */
    public String getStreamWithTimestampUrl(long timestamp) {
        return getPodUrl() + SUBURL_STREAM_WITH_TIMESTAMP + timestamp;
    }

    /**
     * Returns a url that points to the post with the id postId
     *
     * @return https://(pod-domain.tld)/posts/(postId)
     */
    public String getPostsUrl(long postId) {
        return getPodUrl() + SUBURL_POSTS + postId;
    }

    /**
     * Return a url that points to the conversations overview of the registered diaspora account
     *
     * @return https://(pod-domain.tld)/conversations
     */
    public String getConversationsUrl() {
        return getPodUrl() + SUBURL_CONVERSATIONS;
    }

    /**
     * Return a url that points to the new-post form that lets the user create a new post
     *
     * @return https://(pod-domain.tld)/status_messages/new
     */
    public String getNewPostUrl() {
        return getPodUrl() + SUBURL_NEW_POST;
    }

    /**
     * Return a url that shows the profile of the currently registered diaspora account
     *
     * @return https://(hub-domain.tld)/profiles
     */
    public String getProfileUrl() {
        return getPodUrl() + SUBURL_API_PROFILE;
    }

    /**
     * Return a url that shows the Events of the currently registered Hubzilla account
     *
     * @return https://(hub-domain.tld)/events
     */
    public String getEventsUrl() {
        return getPodUrl() + SUBURL_EVENTS;
    }

    /**
     * Return a url that shows the profile of the user with user id profileId
     *
     * @param profileId Id of the profile to be shown
     * @return https://(hub-domain.tld)/profiles
     */
    public String getProfileUrl(String profileId) {
        return getPodUrl() + SUBURL_PEOPLE + profileId;
    }

    /**
     * Return a url that points to the activities feed of the currently registered diaspora account
     *
     * @return https://(pod-domain.tld)/activity
     */
    public String getActivityUrl() {
        return getPodUrl() + SUBURL_ACTIVITY;
    }


   /**
     * Return a url that points to the stream of public posts
     *
     * @return https://(pod-domain.tld)/public
     */
    public String getPubstreamUrl() {
        return getPodUrl() + SUBURL_PUBSTREAM;
    }

    /**
     * Return a url that queries posts for the given hashtag query
     *
     * @param query hashtag to be searched
     * @return https://(hub-domain.tld)/tags/query
     */
    public String getSearchTagsUrl(String query) {
        return getPodUrl() + SUBURL_SEARCH_TAGS + query;
    }

    /**
     * Return a url that queries user accounts for query
     *
     * @param query search term
     * @return https://(hub-domain.tld)/people.mobile?q=(query)
     */
    public String getSearchPeopleUrl(String query) {
        return getPodUrl() + SUBURL_SEARCH_PEOPLE + query;
    }

    /**
     * Return a url that queries user accounts for query
     *
     * @param query search term
     * @return https://(hub-domain.tld)/search?search=(query)
     */
    public String getSearchContentUrl(String query) {
        return getPodUrl() + SUBURL_SEARCH_CONTENT + query;
    }


    /**
     * Return a url that points to the sign in page of the pod.
     *
     * @return https://(pod-domain.tld)/users/sign_in
     */
    public String getSignInUrl() {
        return getPodUrl() + SUBURL_SIGN_IN;
    }

    /**
     * Return a url that points to the personal settings page of the pod.
     *
     * @return https://(pod-domain.tld)/user/edit
     */
    public String getPersonalSettingsUrl() {
        return getPodUrl() + SUBURL_PERSONAL_SETTINGS;
    }

    /**
     * Return a url that points to the addon settings page of the Hub.
     *
     * @return https://(pod-domain.tld)/settings/featured
     */
    public String getAddonUrl() {
        return getPodUrl() + SUBURL_MANAGE_ADDON;
    }

    /**
     * Return a url that points to the suggestions page of the Hub.
     *
     * @return https://(pod-domain.tld)/settings/featured
     */
    public String getSuggestionsUrl() {
        return getPodUrl() + SUBURL_SUGGESTIONS;
    }

    /**
     * Return a url that points to the Directory overview of the registered Hubzilla account
     *
     * @return https://(hub-domain.tld)/directory
     */

    public String getDirectoryUrl() {
        return getPodUrl() + SUBURL_DIRECTORY;
    }

        /**
         * Return a url that points to the Channel home overview of the registered diaspora account
         *
         * @return https://(pod-domain.tld)/channel
         */
        public String getHomeUrl() {
            return getPodUrl() + SUBURL_HOME;
        }


    /**
     * Return a url that points to the manage tags page of the pod.
     *
     * @return https://(pod-domain.tld)/contacts
     */
    public String getContactsUrl() {
        return getPodUrl() + SUBURL_CONTACTS;
    }

    /**
     * Return a url that points to the manage tags page of the pod.
     *
     * @return https://(pod-domain.tld)/contacts
     */
    public String getThemeUrl() {
        return getPodUrl() + SUBURL_THEME;
    }
   /**
     * Returns the url of the blank WebView
     *
     * @return about:blank
     */
    public String getBlankUrl() {
        return URL_BLANK;
    }
//added in Nomad
    public String getInviteUrl() {
        return getPodUrl() + SUBURL_INVITE;
    }


    public String  getApiUrl() {
        return getPodUrl() + SUBURL_API;
    }
}
